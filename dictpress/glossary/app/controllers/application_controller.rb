class ApplicationController < ActionController::Base
  before_action :load_entries
  before_action :paginate

  def index
    render
  end

  private

    def load_entries
      @query_param = params["q"]
      if @query_param.present?
        malayalam_entry_ids =
          Entry.malayalam.where("content LIKE ?", "%#{@query_param}%").pluck(:id)

        non_malayalam_search_result_entry_ids =
          Entry.where(lang: Entry::LANGS.excluding(:malayalam)).where("content LIKE ?", "%#{@query_param}%").pluck(:id)
        non_malayalam_entry_ids =
          Relation.where(to_id: non_malayalam_search_result_entry_ids).pluck(:from_id)

        @entries = Entry.malayalam.where(
          id: [*malayalam_entry_ids, *non_malayalam_entry_ids].uniq
        )
      else
        @entries = Entry.malayalam.select(:id, :content, :lang).limit(15)
      end
    end

    def paginate
      @entries = @entries.paginate(page: params[:page], per_page: 30)
    end
end
